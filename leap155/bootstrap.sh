#!/bin/bash

#
# This file is generated by 'bootstrap/template.py --render'
# See also bootstrap/config.py
#

set -xueo pipefail

zypper --non-interactive refresh
zypper --non-interactive update
zypper --non-interactive install \
    --no-recommends \
    system-user-nobody \
    acl \
    attr \
    autoconf \
    bind-utils \
    binutils \
    bison \
    ccache \
    chrpath \
    cups-devel \
    curl \
    dbus-1-devel \
    docbook-xsl-stylesheets \
    flex \
    gawk \
    gcc \
    gdb \
    git \
    glib2-devel \
    glibc-locale \
    glusterfs-devel \
    gzip \
    jq \
    hostname \
    htop \
    keyutils-devel \
    krb5-devel \
    krb5-server \
    krb5-client \
    krb5-plugin-preauth-pkinit \
    lcov \
    libacl-devel \
    libarchive-devel \
    libattr-devel \
    libavahi-devel \
    libblkid-devel \
    libbsd-devel \
    libcap-devel \
    libcephfs-devel \
    libgnutls-devel \
    libgpgme-devel \
    libicu-devel \
    libjansson-devel \
    libpcap-devel \
    libtasn1-devel \
    libtirpc-devel \
    libunwind-devel \
    liburing-devel \
    libuuid-devel \
    lmdb \
    lmdb-devel \
    lsb-release \
    make \
    mingw64-gcc \
    ncurses-devel \
    openldap2-devel \
    pam-devel \
    patch \
    perl \
    perl-Archive-Tar-Wrapper \
    perl-ExtUtils-MakeMaker \
    perl-JSON \
    perl-JSON-XS \
    perl-Parse-Yapp \
    perl-Test-Base \
    pkgconfig \
    popt-devel \
    procps \
    psmisc \
    python3 \
    python3-Markdown \
    python3-cryptography \
    python3-devel \
    python3-dnspython \
    python3-gpg \
    python3-iso8601 \
    python3-policycoreutils \
    python3-pyasn1 \
    python3-semanage \
    python3-setproctitle \
    python3-xml \
    readline-devel \
    rng-tools \
    rpcgen \
    rpcsvc-proto-devel \
    rsync \
    sed \
    sudo \
    systemd-devel \
    tar \
    tracker-devel \
    tree \
    wget \
    which \
    xfsprogs-devel \
    zlib-devel

zypper --non-interactive install --no-recommends --allow-vendor-change \
        diffutils               \
        git-core                \
        python3-chardet       \
        python3-pycryptodome  \
        python3-requests  \
        valgrind-devel          \
        libgnutls30-hmac        \
        libopenssl1_1-hmac

zypper --non-interactive install -t pattern fips

zypper --non-interactive clean

if [ -f /usr/lib/mit/bin/krb5-config ]; then
    ln -sf /usr/lib/mit/bin/krb5-config /usr/bin/krb5-config
fi

